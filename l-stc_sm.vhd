----------------------gaiyou------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    18:27:56 09/12/2012
-- Design Name:
-- Module Name:    kamalab - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kamalab is
  port (CLK_ip  : in  std_logic;
        LED_out : out std_logic_vector (63 downto 0));
end kamalab;


architecture Behavioral of kamalab is


  signal BARKER : std_logic_vector(14 downto 0);
  signal PN     : std_logic_vector(100 downto 0);

  signal CLK            : std_logic;
  shared variable count : integer := 0;
  shared variable i     : integer := 0;
  shared variable j     : integer := 0;
  shared variable k     : integer := 0;
  shared variable p     : integer := 3;
  shared variable q     : integer := 4;
  shared variable r     : integer := 5;
  shared variable x     : integer := 0;
  shared variable e     : integer := 1;
  shared variable f     : integer := 2;
  shared variable smB   : integer := 0;
  shared variable smC   : integer := 0;
  shared variable smD   : integer := 0;
  shared variable state : integer := 0;

  signal LED : std_logic_vector(63 downto 0);

begin

--10101100111110

  CLK     <= CLK_ip;
  LED_out <= LED;

  BARKER(0)  <= '1'; BARKER(1) <= '0';
  BARKER(2)  <= '1'; BARKER(3) <= '0';
  BARKER(4)  <= '1'; BARKER(5) <= '1';
  BARKER(6)  <= '0'; BARKER(7) <= '0';
  BARKER(8)  <= '1'; BARKER(9) <= '1';
  BARKER(10) <= '1'; BARKER(11) <= '1';
  BARKER(12) <= '1'; BARKER(13) <= '0';
  BARKER(14) <= '0';


  PN(0)  <= '0'; PN(1) <= '1'; PN(2) <= '0'; PN(3) <= '0'; PN(4) <= '1';
  PN(5)  <= '1'; PN(6) <= '1'; PN(7) <= '1'; PN(8) <= '0'; PN(9) <= '1';
  PN(10) <= '0'; PN(11) <= '0'; PN(12) <= '1'; PN(13) <= '1'; PN(14) <= '0';
  PN(15) <= '1'; PN(16) <= '1'; PN(17) <= '1'; PN(18) <= '1'; PN(19) <= '0';
  PN(20) <= '0'; PN(21) <= '0'; PN(22) <= '1'; PN(23) <= '1'; PN(24) <= '0';
  PN(25) <= '0'; PN(26) <= '0'; PN(27) <= '0'; PN(28) <= '0'; PN(29) <= '1';
  PN(30) <= '1'; PN(31) <= '1'; PN(32) <= '1'; PN(33) <= '1'; PN(34) <= '1';
  PN(35) <= '1'; PN(36) <= '0'; PN(37) <= '0'; PN(38) <= '1'; PN(39) <= '1';
  PN(40) <= '0'; PN(41) <= '0'; PN(42) <= '1'; PN(43) <= '0'; PN(44) <= '1';
  PN(45) <= '1'; PN(46) <= '0'; PN(47) <= '1'; PN(48) <= '0'; PN(49) <= '0';
  PN(50) <= '0'; PN(51) <= '1'; PN(52) <= '1'; PN(53) <= '1'; PN(54) <= '0';
  PN(55) <= '1'; PN(56) <= '0'; PN(57) <= '1'; PN(58) <= '1'; PN(59) <= '0';
  PN(60) <= '0'; PN(61) <= '0'; PN(62) <= '1'; PN(63) <= '0'; PN(64) <= '0';
  PN(65) <= '1'; PN(66) <= '0'; PN(67) <= '1'; PN(68) <= '0'; PN(69) <= '0';
  PN(70) <= '0'; PN(71) <= '0'; PN(72) <= '1'; PN(73) <= '0'; PN(74) <= '0';
  PN(75) <= '0'; PN(76) <= '0'; PN(77) <= '0'; PN(78) <= '0'; PN(79) <= '1';
  PN(80) <= '0'; PN(81) <= '1'; PN(82) <= '0'; PN(83) <= '1'; PN(84) <= '0';
  PN(85) <= '1'; PN(86) <= '1'; PN(87) <= '1'; PN(88) <= '0'; PN(89) <= '1';
  PN(90) <= '1'; PN(91) <= '1'; PN(92) <= '0';
  PN(93) <= '0'; PN(94) <= '1'; PN(95) <= '0'; PN(96) <= '0'; PN(97) <= '1';
  PN(98) <= '1'; PN(99) <= '1'; PN(100) <= '1';

  process(CLK)begin
    if(CLK'event and CLK = '1')then
      count := count + 1;
			--50000000   1[Hz]
			--50000      1[kHz]

      if(count = 5000000)then  --1[kHz](now)
        count := 0;

-------------------------------------------------------------BARKER送信開始
        if(state = 0) then
          for i in 1 to 62 loop
            LED(i) <= BARKER(j);
          end loop;
          LED(63) <= '0';
          LED(0)  <= '0';
          j       := j+1;
          if(j = 15)then
            j     := 0;
            state := 3;
          end if;

        elsif(state = 2)then

          if(k = 0) then
            for i in 0 to 3 loop
              LED(0+i)  <= '1';
              LED(8+i)  <= '1';
              LED(16+i) <= '1';
              LED(24+i) <= '1';
            end loop;

            k := k+1;

          elsif(k = 1) then

            for i in 0 to 3 loop
              LED(0+i)  <= '0';
              LED(8+i)  <= '0';
              LED(16+i) <= '0';
              LED(24+i) <= '0';

              LED(4+i)  <= '1';
              LED(12+i) <= '1';
              LED(20+i) <= '1';
              LED(28+i) <= '1';

            end loop;

            k := k+1;

          elsif(k = 2) then

            for i in 0 to 3 loop
              LED(4+i)  <= '0';
              LED(12+i) <= '0';
              LED(20+i) <= '0';
              LED(28+i) <= '0';

              LED(32+i) <= '1';
              LED(40+i) <= '1';
              LED(48+i) <= '1';
              LED(56+i) <= '1';

            end loop;

            k := k+1;

          elsif(k = 3) then

            for i in 0 to 3 loop
              LED(32+i) <= '0';
              LED(40+i) <= '0';
              LED(48+i) <= '0';
              LED(56+i) <= '0';

              LED(36+i) <= '1';
              LED(44+i) <= '1';
              LED(52+i) <= '1';
              LED(60+i) <= '1';

            end loop;

            k := k+1;

          elsif(k = 4) then

            for i in 0 to 3 loop
              LED(36+i) <= '0';
              LED(44+i) <= '0';
              LED(52+i) <= '0';
              LED(60+i) <= '0';
            end loop;

            k     := 0;
            state := 3;
          end if;


        elsif(state = 3) then
          for i in 1 to 62 loop
            LED(i) <= '0';
          end loop;

          LED(63) <= '0';
          LED(0)  <= '0';
          j       := j+1;
          if(j = 15)then
            k     := 0;
            j     := 0;
            state := 1;
          end if;

--------------------------------------------------------------PN送信開始

        elsif(state = 1)then
          if(k >= 93) then
            k := k-93;
          end if;
          if(e >= 93) then
            e := e-93;
          end if;
          if(f >= 93) then
            f := f-93;
          end if;

          if(p >= 93) then
            p := p-93;
          end if;

--------------------------------------------------------------Spatial modulation
          if(x mod 2 = 0)then
            if(PN(p) = '0') then
              smB := 0;
            else
              smB := 1;
            end if;

            if(PN(q) = '0') then
              smC := 0;
            else
              smC := 1;
            end if;


          end if;
----------------------------------------------------------------------------------------------


          if(smB = 0) then
            if(smC = 0)then
----------------------------------------------------smB=0 B層　
              if(x mod 2 = 0) then
                LED(0)  <= PN(k);
                LED(1)  <= PN(k);
                LED(2)  <= PN(k);
                LED(3)  <= PN(k);
                LED(8)  <= PN(k);
                LED(9)  <= PN(k);
                LED(10) <= PN(k);
                LED(11) <= PN(k);
                LED(16) <= PN(k);
                LED(17) <= PN(k);
                LED(18) <= PN(k);
                LED(19) <= PN(k);
                LED(24) <= PN(k);
                LED(25) <= PN(k);
                LED(26) <= PN(k);
                LED(27) <= PN(k);

                LED(36) <= PN(k+1);
                LED(37) <= PN(k+1);
                LED(38) <= PN(k+1);
                LED(39) <= PN(k+1);
                LED(44) <= PN(k+1);
                LED(45) <= PN(k+1);
                LED(46) <= PN(k+1);
                LED(47) <= PN(k+1);
                LED(52) <= PN(k+1);
                LED(53) <= PN(k+1);
                LED(54) <= PN(k+1);
                LED(55) <= PN(k+1);
                LED(60) <= PN(k+1);
                LED(61) <= PN(k+1);
                LED(62) <= PN(k+1);
                LED(63) <= PN(k+1);
-------------------------------------------------------------------------------

                LED(4)  <= not PN(e);
                LED(5)  <= not PN(e);
                LED(12) <= not PN(e);
                LED(13) <= not PN(e);

                LED(22) <= not PN(e+1);
                LED(23) <= not PN(e+1);
                LED(30) <= not PN(e+1);
                LED(31) <= not PN(e+1);

                LED(32) <= PN(e);
                LED(33) <= PN(e);
                LED(40) <= PN(e);
                LED(41) <= PN(e);

                LED(50) <= PN(e+1);
                LED(51) <= PN(e+1);
                LED(58) <= PN(e+1);
                LED(59) <= PN(e+1);

                --below
                LED(6)  <= PN(f);
                LED(7)  <= '0';
                LED(14) <= '1';
                LED(15) <= PN(f+1);

                LED(20) <= not PN(f);
                LED(21) <= '1';
                LED(28) <= '0';
                LED(29) <= not PN(f+1);

                LED(34) <= not PN(f);
                LED(35) <= '1';
                LED(42) <= '0';
                LED(43) <= not PN(f+1);

                LED(48) <= PN(f);
                LED(49) <= '0';
                LED(56) <= '1';
                LED(57) <= PN(f+1);


              else
                LED(0)  <= not PN(k+1);
                LED(1)  <= not PN(k+1);
                LED(2)  <= not PN(k+1);
                LED(3)  <= not PN(k+1);
                LED(8)  <= not PN(k+1);
                LED(9)  <= not PN(k+1);
                LED(10) <= not PN(k+1);
                LED(11) <= not PN(k+1);
                LED(16) <= not PN(k+1);
                LED(17) <= not PN(k+1);
                LED(18) <= not PN(k+1);
                LED(19) <= not PN(k+1);
                LED(24) <= not PN(k+1);
                LED(25) <= not PN(k+1);
                LED(26) <= not PN(k+1);
                LED(27) <= not PN(k+1);

                LED(36) <= PN(k);
                LED(37) <= PN(k);
                LED(38) <= PN(k);
                LED(39) <= PN(k);
                LED(44) <= PN(k);
                LED(45) <= PN(k);
                LED(46) <= PN(k);
                LED(47) <= PN(k);
                LED(52) <= PN(k);
                LED(53) <= PN(k);
                LED(54) <= PN(k);
                LED(55) <= PN(k);
                LED(60) <= PN(k);
                LED(61) <= PN(k);
                LED(62) <= PN(k);
                LED(63) <= PN(k);
----------------------------------------------------------

                LED(4)  <= PN(e+1);
                LED(5)  <= PN(e+1);
                LED(12) <= PN(e+1);
                LED(13) <= PN(e+1);

                LED(22) <= not PN(e);
                LED(23) <= not PN(e);
                LED(30) <= not PN(e);
                LED(31) <= not PN(e);

                LED(32) <= not PN(e+1);
                LED(33) <= not PN(e+1);
                LED(40) <= not PN(e+1);
                LED(41) <= not PN(e+1);

                LED(50) <= PN(e);
                LED(51) <= PN(e);
                LED(58) <= PN(e);
                LED(59) <= PN(e);

                LED(6)  <= not PN(f+1);
                LED(7)  <= '1';
                LED(14) <= '0';
                LED(15) <= PN(f);

                LED(20) <= PN(f+1);
                LED(21) <= '0';
                LED(28) <= '1';
                LED(29) <= not PN(f);

                LED(34) <= PN(f+1);
                LED(35) <= '0';
                LED(42) <= '1';
                LED(43) <= not PN(f);

                LED(48) <= not PN(f+1);
                LED(49) <= '1';
                LED(56) <= '0';
                LED(57) <= PN(f);

-------------------------------------------------------

              end if;  --x end

            elsif(smC = 1)then
              if(x mod 2 = 0) then
                LED(0)  <= PN(k);
                LED(1)  <= PN(k);
                LED(2)  <= PN(k);
                LED(3)  <= PN(k);
                LED(8)  <= PN(k);
                LED(9)  <= PN(k);
                LED(10) <= PN(k);
                LED(11) <= PN(k);
                LED(16) <= PN(k);
                LED(17) <= PN(k);
                LED(18) <= PN(k);
                LED(19) <= PN(k);
                LED(24) <= PN(k);
                LED(25) <= PN(k);
                LED(26) <= PN(k);
                LED(27) <= PN(k);

                LED(36) <= PN(k+1);
                LED(37) <= PN(k+1);
                LED(38) <= PN(k+1);
                LED(39) <= PN(k+1);
                LED(44) <= PN(k+1);
                LED(45) <= PN(k+1);
                LED(46) <= PN(k+1);
                LED(47) <= PN(k+1);
                LED(52) <= PN(k+1);
                LED(53) <= PN(k+1);
                LED(54) <= PN(k+1);
                LED(55) <= PN(k+1);
                LED(60) <= PN(k+1);
                LED(61) <= PN(k+1);
                LED(62) <= PN(k+1);
                LED(63) <= PN(k+1);
-------------------------------------------------------------------------------

                LED(6)  <= not PN(e);
                LED(7)  <= not PN(e);
                LED(14) <= not PN(e);
                LED(15) <= not PN(e);

                LED(20) <= not PN(e+1);
                LED(21) <= not PN(e+1);
                LED(28) <= not PN(e+1);
                LED(29) <= not PN(e+1);

                LED(34) <= PN(e);
                LED(35) <= PN(e);
                LED(42) <= PN(e);
                LED(43) <= PN(e);

                LED(48) <= PN(e+1);
                LED(49) <= PN(e+1);
                LED(56) <= PN(e+1);
                LED(57) <= PN(e+1);

                --below
                LED(4)  <= PN(f);
                LED(5)  <= '0';
                LED(12) <= '1';
                LED(13) <= PN(f+1);

                LED(22) <= not PN(f);
                LED(23) <= '1';
                LED(30) <= '0';
                LED(31) <= not PN(f+1);

                LED(32) <= not PN(f);
                LED(33) <= '1';
                LED(40) <= '0';
                LED(41) <= not PN(f+1);

                LED(50) <= PN(f);
                LED(51) <= '0';
                LED(58) <= '1';
                LED(59) <= PN(f+1);


              else
                LED(0)  <= not PN(k+1);
                LED(1)  <= not PN(k+1);
                LED(2)  <= not PN(k+1);
                LED(3)  <= not PN(k+1);
                LED(8)  <= not PN(k+1);
                LED(9)  <= not PN(k+1);
                LED(10) <= not PN(k+1);
                LED(11) <= not PN(k+1);
                LED(16) <= not PN(k+1);
                LED(17) <= not PN(k+1);
                LED(18) <= not PN(k+1);
                LED(19) <= not PN(k+1);
                LED(24) <= not PN(k+1);
                LED(25) <= not PN(k+1);
                LED(26) <= not PN(k+1);
                LED(27) <= not PN(k+1);

                LED(36) <= PN(k);
                LED(37) <= PN(k);
                LED(38) <= PN(k);
                LED(39) <= PN(k);
                LED(44) <= PN(k);
                LED(45) <= PN(k);
                LED(46) <= PN(k);
                LED(47) <= PN(k);
                LED(52) <= PN(k);
                LED(53) <= PN(k);
                LED(54) <= PN(k);
                LED(55) <= PN(k);
                LED(60) <= PN(k);
                LED(61) <= PN(k);
                LED(62) <= PN(k);
                LED(63) <= PN(k);
----------------------------------------------------------

                LED(6)  <= PN(e+1);
                LED(7)  <= PN(e+1);
                LED(14) <= PN(e+1);
                LED(15) <= PN(e+1);

                LED(20) <= not PN(e);
                LED(21) <= not PN(e);
                LED(28) <= not PN(e);
                LED(29) <= not PN(e);

                LED(34) <= not PN(e+1);
                LED(35) <= not PN(e+1);
                LED(42) <= not PN(e+1);
                LED(43) <= not PN(e+1);

                LED(48) <= PN(e);
                LED(49) <= PN(e);
                LED(56) <= PN(e);
                LED(57) <= PN(e);

                LED(4)  <= not PN(f+1);
                LED(5)  <= '1';
                LED(12) <= '0';
                LED(13) <= PN(f);

                LED(22) <= PN(f+1);
                LED(23) <= '0';
                LED(30) <= '1';
                LED(31) <= not PN(f);

                LED(32) <= PN(f+1);
                LED(33) <= '0';
                LED(40) <= '1';
                LED(41) <= not PN(f);

                LED(50) <= not PN(f+1);
                LED(51) <= '1';
                LED(58) <= '0';
                LED(59) <= PN(f);
              end if;  --x
            end if;  --smC
----------------------------------------------------smB=1 B層　

          elsif(smB = 1) then

----------------------------------------------------smB=2 B層　

            if(smC = 0)then
              if(x mod 2 = 0) then
                LED(4)  <= PN(k);
                LED(5)  <= PN(k);
                LED(6)  <= PN(k);
                LED(7)  <= PN(k);
                LED(12) <= PN(k);
                LED(13) <= PN(k);
                LED(14) <= PN(k);
                LED(15) <= PN(k);
                LED(20) <= PN(k);
                LED(21) <= PN(k);
                LED(22) <= PN(k);
                LED(23) <= PN(k);
                LED(28) <= PN(k);
                LED(29) <= PN(k);
                LED(30) <= PN(k);
                LED(31) <= PN(k);

                LED(32) <= PN(k+1);
                LED(33) <= PN(k+1);
                LED(34) <= PN(k+1);
                LED(35) <= PN(k+1);
                LED(40) <= PN(k+1);
                LED(41) <= PN(k+1);
                LED(42) <= PN(k+1);
                LED(43) <= PN(k+1);
                LED(48) <= PN(k+1);
                LED(49) <= PN(k+1);
                LED(50) <= PN(k+1);
                LED(51) <= PN(k+1);
                LED(56) <= PN(k+1);
                LED(57) <= PN(k+1);
                LED(58) <= PN(k+1);
                LED(59) <= PN(k+1);
-------------------------------------------------------------------------


                LED(0) <= not PN(e);
                LED(1) <= not PN(e);
                LED(8) <= not PN(e);
                LED(9) <= not PN(e);

                LED(18) <= not PN(e+1);
                LED(19) <= not PN(e+1);
                LED(26) <= not PN(e+1);
                LED(27) <= not PN(e+1);

                LED(36) <= PN(e);
                LED(37) <= PN(e);
                LED(44) <= PN(e);
                LED(45) <= PN(e);

                LED(54) <= PN(e+1);
                LED(55) <= PN(e+1);
                LED(62) <= PN(e+1);
                LED(63) <= PN(e+1);

                LED(2)  <= PN(f);
                LED(3)  <= '0';
                LED(10) <= '1';
                LED(11) <= PN(f+1);

                LED(16) <= not PN(f);
                LED(17) <= '1';
                LED(24) <= '0';
                LED(25) <= not PN(f+1);

                LED(38) <= not PN(f);
                LED(39) <= '1';
                LED(46) <= '0';
                LED(47) <= not PN(f+1);

                LED(52) <= PN(f);
                LED(53) <= '0';
                LED(60) <= '1';
                LED(61) <= PN(f+1);

              else
                LED(4)  <= not PN(k+1);
                LED(5)  <= not PN(k+1);
                LED(6)  <= not PN(k+1);
                LED(7)  <= not PN(k+1);
                LED(12) <= not PN(k+1);
                LED(13) <= not PN(k+1);
                LED(14) <= not PN(k+1);
                LED(15) <= not PN(k+1);
                LED(20) <= not PN(k+1);
                LED(21) <= not PN(k+1);
                LED(22) <= not PN(k+1);
                LED(23) <= not PN(k+1);
                LED(28) <= not PN(k+1);
                LED(29) <= not PN(k+1);
                LED(30) <= not PN(k+1);
                LED(31) <= not PN(k+1);

                LED(32) <= PN(k);
                LED(33) <= PN(k);
                LED(34) <= PN(k);
                LED(35) <= PN(k);
                LED(40) <= PN(k);
                LED(41) <= PN(k);
                LED(42) <= PN(k);
                LED(43) <= PN(k);
                LED(48) <= PN(k);
                LED(49) <= PN(k);
                LED(50) <= PN(k);
                LED(51) <= PN(k);
                LED(56) <= PN(k);
                LED(57) <= PN(k);
                LED(58) <= PN(k);
                LED(59) <= PN(k);
--------------------------------------------------------------

                LED(0) <= PN(e+1);
                LED(1) <= PN(e+1);
                LED(8) <= PN(e+1);
                LED(9) <= PN(e+1);

                LED(18) <= not PN(e);
                LED(19) <= not PN(e);
                LED(26) <= not PN(e);
                LED(27) <= not PN(e);

                LED(36) <= not PN(e+1);
                LED(37) <= not PN(e+1);
                LED(44) <= not PN(e+1);
                LED(45) <= not PN(e+1);

                LED(54) <= PN(e);
                LED(55) <= PN(e);
                LED(62) <= PN(e);
                LED(63) <= PN(e);

                LED(2)  <= not PN(f+1);
                LED(3)  <= '1';
                LED(10) <= '0';
                LED(11) <= PN(f);

                LED(16) <= PN(f+1);
                LED(17) <= '0';
                LED(24) <= '1';
                LED(25) <= not PN(f);

                LED(38) <= PN(f+1);
                LED(39) <= '0';
                LED(46) <= '1';
                LED(47) <= not PN(f);

                LED(52) <= not PN(f+1);
                LED(53) <= '1';
                LED(60) <= '0';
                LED(61) <= PN(f);

              end if;  -- x end
----------------------------------------------------smB=2 B層　
            elsif(smC = 1)then
              if(x mod 2 = 0) then
                LED(4)  <= PN(k);
                LED(5)  <= PN(k);
                LED(6)  <= PN(k);
                LED(7)  <= PN(k);
                LED(12) <= PN(k);
                LED(13) <= PN(k);
                LED(14) <= PN(k);
                LED(15) <= PN(k);
                LED(20) <= PN(k);
                LED(21) <= PN(k);
                LED(22) <= PN(k);
                LED(23) <= PN(k);
                LED(28) <= PN(k);
                LED(29) <= PN(k);
                LED(30) <= PN(k);
                LED(31) <= PN(k);

                LED(32) <= PN(k+1);
                LED(33) <= PN(k+1);
                LED(34) <= PN(k+1);
                LED(35) <= PN(k+1);
                LED(40) <= PN(k+1);
                LED(41) <= PN(k+1);
                LED(42) <= PN(k+1);
                LED(43) <= PN(k+1);
                LED(48) <= PN(k+1);
                LED(49) <= PN(k+1);
                LED(50) <= PN(k+1);
                LED(51) <= PN(k+1);
                LED(56) <= PN(k+1);
                LED(57) <= PN(k+1);
                LED(58) <= PN(k+1);
                LED(59) <= PN(k+1);
-------------------------------------------------------------------------


                LED(2)  <= not PN(e);
                LED(3)  <= not PN(e);
                LED(10) <= not PN(e);
                LED(11) <= not PN(e);

                LED(16) <= not PN(e+1);
                LED(17) <= not PN(e+1);
                LED(24) <= not PN(e+1);
                LED(25) <= not PN(e+1);

                LED(38) <= PN(e);
                LED(39) <= PN(e);
                LED(46) <= PN(e);
                LED(47) <= PN(e);

                LED(52) <= PN(e+1);
                LED(53) <= PN(e+1);
                LED(60) <= PN(e+1);
                LED(61) <= PN(e+1);

                LED(0) <= PN(f);
                LED(1) <= '0';
                LED(8) <= '1';
                LED(9) <= PN(f+1);

                LED(18) <= not PN(f);
                LED(19) <= '1';
                LED(26) <= '0';
                LED(27) <= not PN(f+1);

                LED(36) <= not PN(f);
                LED(37) <= '1';
                LED(48) <= '0';
                LED(49) <= not PN(f+1);

                LED(54) <= PN(f);
                LED(55) <= '0';
                LED(62) <= '1';
                LED(63) <= PN(f+1);

              else
                LED(4)  <= not PN(k+1);
                LED(5)  <= not PN(k+1);
                LED(6)  <= not PN(k+1);
                LED(7)  <= not PN(k+1);
                LED(12) <= not PN(k+1);
                LED(13) <= not PN(k+1);
                LED(14) <= not PN(k+1);
                LED(15) <= not PN(k+1);
                LED(20) <= not PN(k+1);
                LED(21) <= not PN(k+1);
                LED(22) <= not PN(k+1);
                LED(23) <= not PN(k+1);
                LED(28) <= not PN(k+1);
                LED(29) <= not PN(k+1);
                LED(30) <= not PN(k+1);
                LED(31) <= not PN(k+1);

                LED(32) <= PN(k);
                LED(33) <= PN(k);
                LED(34) <= PN(k);
                LED(35) <= PN(k);
                LED(40) <= PN(k);
                LED(41) <= PN(k);
                LED(42) <= PN(k);
                LED(43) <= PN(k);
                LED(48) <= PN(k);
                LED(49) <= PN(k);
                LED(50) <= PN(k);
                LED(51) <= PN(k);
                LED(56) <= PN(k);
                LED(57) <= PN(k);
                LED(58) <= PN(k);
                LED(59) <= PN(k);
--------------------------------------------------------------

                LED(2)  <= PN(e+1);
                LED(3)  <= PN(e+1);
                LED(10) <= PN(e+1);
                LED(11) <= PN(e+1);

                LED(16) <= not PN(e);
                LED(17) <= not PN(e);
                LED(24) <= not PN(e);
                LED(25) <= not PN(e);

                LED(38) <= not PN(e+1);
                LED(39) <= not PN(e+1);
                LED(46) <= not PN(e+1);
                LED(47) <= not PN(e+1);

                LED(52) <= PN(e);
                LED(53) <= PN(e);
                LED(60) <= PN(e);
                LED(61) <= PN(e);

                LED(0) <= not PN(f+1);
                LED(1) <= '1';
                LED(8) <= '0';
                LED(9) <= PN(f);

                LED(18) <= PN(f+1);
                LED(19) <= '0';
                LED(26) <= '1';
                LED(27) <= not PN(f);

                LED(36) <= PN(f+1);
                LED(37) <= '0';
                LED(44) <= '1';
                LED(45) <= not PN(f);

                LED(54) <= not PN(f+1);
                LED(55) <= '1';
                LED(62) <= '0';
                LED(63) <= PN(f);

              end if;  -- x end
            end if;  --smC_end
          end if;  --------smB_end

          if(x mod 2 = 1)then
            e := e+2;
            f := f+2;
            k := k+2;
            p := p+1;
            q := q+1;
            r := r+1;
          end if;

          x := x+1;

          if(x >= 1000)then
            x     := 0;
            k     := 0;
            e     := 1;
            f     := 2;
            p     := 3;
            q     := 4;
            r     := 5;
            state := 0;
          end if;
--------------------------------------------------------------
        end if;
      end if;
    end if;
  end process;
end behavioral;
