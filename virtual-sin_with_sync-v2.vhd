----------------------gaiyou------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    18:27:56 09/12/2012
-- Design Name:
-- Module Name:    kamalab - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kamalab is
  port (CLK_ip  : in  std_logic;
        LED_out : out std_logic_vector (63 downto 0));
end kamalab;


architecture Behavioral of kamalab is

  constant M            : integer := 4; -- BPSK=2, QPSK=4
  constant Tx_Hz        : integer := 20; -- Signal Frequency
  constant DATA_LEN     : integer := 110; -- Data Length

  type INT_ARRAY is array(0 to DATA_LEN-1) of integer;
  shared variable DATA  : INT_ARRAY := (0,0,0,0,0,0,0,0,0,0,
                                        2,3,0,0,0,1,1,3,0,2,
                                        0,3,0,2,3,1,0,0,1,1,
                                        0,3,2,3,1,3,0,2,3,0,
                                        3,3,3,2,3,1,0,1,2,3,
                                        1,1,3,2,3,0,3,2,0,1,
                                        1,2,3,0,3,3,1,1,2,2,
                                        2,3,1,1,3,2,3,1,2,2,
                                        3,0,2,2,0,3,3,1,3,0,
                                        2,1,1,3,0,1,1,1,3,3,
                                        0,0,2,1,3,3,2,3,1,2);

--  signal BASE_WAVE    : std_logic_vector(0 to M*3-1) := "111000"; -- for BPSK
  signal BASE_WAVE    : std_logic_vector(0 to M*3-1) := "111111000000"; -- for QPSK
--  signal BASE_WAVE    : std_logic_vector(0 to M*3-1) := "111111111000000000"; -- for 6PSK
--  signal BASE_WAVE    : std_logic_vector(0 to M*3-1) := "111111111111000000000000"; -- for 8PSK

  signal CLK            : std_logic;
  shared variable count : integer := 0;
  shared variable symbol_end     : integer := 0;
  shared variable i     : integer := 0;
  shared variable j     : integer := 0;
  shared variable k     : integer := -1;
  shared variable z     : integer := 0;
  shared variable tmp     : integer := 0;

  signal LED : std_logic_vector(63 downto 0);

begin

  CLK     <= CLK_ip;
  LED_out <= LED;

  process(CLK)begin
    if(CLK'event and CLK = '1')then
      count := count + 1;
      --50000000   1[Hz]
      --50000      1[kHz]

      if(count = (50000000/(Tx_Hz*M*3)))then  -- 送信周波数をM*3分割（多値数xフレーム数）
        count := 0;

        if(k = -1) then
          k := 3*DATA(j);
          symbol_end := 4*M;-- データ+ガードフレーム
        end if;

        tmp := tmp+1;

        -- -- Lチカ
        -- for i in 1 to 31 loop
        --   LED(i) <= BASE_WAVE(k);
        -- end loop;
        --  -- Lチカ
        -- for i in 32 to 62 loop
        --   LED(i) <= not LED(i);
        -- end loop;

        LED(18) <= BASE_WAVE(k);
        LED(21) <= BASE_WAVE(k);
        LED(42) <= BASE_WAVE(k);
        LED(45) <= BASE_WAVE(k);

        k := k + 1;
        if(k >= M*3) then
          k := 0;
        end if;

        if(tmp=symbol_end) then
          tmp := 0;
          k := -1;
          j := j+1;
          if(j >= DATA_LEN) then -- dataの送信が終了
            j := 0;
          end if;
        end if;

        -- クロック周波数が割り切れないので，シンボル終わり（GF手前）でずれた分を補正
        z := z + 1;
        if(z = M*3) then
          z := 0;
          --count := -2; -- M=2 (50000000/(Tx_Hz)*M*3) * M*3 - 50000000/Tx_Hz;
          count := -8; -- M=4
          --count := -14;-- M=6
          --count := -8; -- M=8
        end if;
        -- ↑ 差動復号なら前フレームとの比較なので，信号1シンボルとフレーム3枚分の時間が一致していなくても大丈夫そう

      end if; -- 分周end
    end if; -- clock end
  end process;
end Behavioral;
