----------------------gaiyou------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    18:27:56 09/12/2012
-- Design Name:
-- Module Name:    kamalab - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity kamalab is
  port (CLK_ip  : in  std_logic;
        LED_out : out std_logic_vector (63 downto 0));
end kamalab;


architecture Behavioral of kamalab is
  -- signal BARKER : std_logic_vector(14 downto 0);

  signal PREAMBLE : std_logic_vector(9 downto 0) := "1111111100";
  signal DATA     : std_logic_vector(15 downto 0) := "1100100100110110";

  signal CLK            : std_logic;
  shared variable count : integer := 0;
  shared variable i     : integer := 0;
  shared variable j     : integer := 0;
  -- shared variable k     : integer := 0;
  -- shared variable p     : integer := 3;
  -- shared variable q     : integer := 4;
  -- shared variable r     : integer := 5;
  -- shared variable x     : integer := 0;
  -- shared variable e     : integer := 1;
  -- shared variable f     : integer := 2;
  -- shared variable smB   : integer := 0;
  -- shared variable smC   : integer := 0;
  -- shared variable smD   : integer := 0;
  shared variable state : integer := 0;

  constant M            : integer := 4; -- BPSK=2, QPSK=4
  constant Tx_Hz        : integer := 1; -- Signal Frequency

  signal LED : std_logic_vector(63 downto 0);

begin

  CLK     <= CLK_ip;
  LED_out <= LED;

  process(CLK)begin
    if(CLK'event and CLK = '1')then
      count := count + 1;
      --50000000   1[Hz]
      --50000      1[kHz]

      if(state = 0) then -- preambleの送信state
        if(count = (50000000)/Tx_Hz / 3) then
          count := 0;

          for i in 1 to 62 loop
            LED(i) <= PREAMBLE(j);  -- preambleの送信
          end loop;

          j := j+1;
          if(j >= 10) then -- preambleの送信が終わったら次状態へ移行
            j := 0;
            state = 1;
          end if;
        end if;

      elsif(state = 1) then -- preambleの最後のON/OFFをLEDに流した後，カメラのフレーム時間分待機
        if(count = (50000000)/Tx_Hz / 3) then
          count := (50000000/Tx_Hz) / M - 1; -- 次のクロックinですぐデータを送信できるようにcountをセット
          state = 2;
        end if;

      elsif(state = 2) then -- dataの送信state
        if(count = (50000000/Tx_Hz) / M)then  -- 送信周波数をM分割
          count := 0;

          -- Lチカ
          for i in 1 to 31 loop
            LED(i) <= DATA(j);
          end loop;

          for i in 32 to 62 loop
            LED(i) <= not LED(i);
          end loop;

          j := j+1;
          if(j >= 16) then -- dataの送信が終わったらプリアンブルの送信に移行
            j := 0;
            state = 0;
          end if;

        end if; -- 分周end
      end if; -- state end
    end if; -- clock end
  end process;
end Behavioral;
